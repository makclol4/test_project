﻿using atFrameWork2.BaseFramework;
using atFrameWork2.PageObjects;

namespace ATframework3demo.TestCases.Calendar
{
    public class Case_Bitrix24_Calendar_Daily : CaseCollectionBuilder
    {
        protected override List<TestCase> GetCases()
        {
            var caseCollection = new List<TestCase>();
            caseCollection.Add(new TestCase("Создание повторяющегося календаря", homePage => CreataCalendarDaily(homePage)));
            return caseCollection;
        }
        void CreataCalendarDaily (PortalHomePage homePage)
        {
            //Проверить название календаря
            //Проверить наличие календаря обновив страницу
            //код кейса здорового человека:
            homePage.
                LeftMenu.
                //Перейти в календарь
                OpenCalendar().
                //Создать календарь
                AddNewCalendar().
                CreateCalendarDaily();
        }
    }
}
