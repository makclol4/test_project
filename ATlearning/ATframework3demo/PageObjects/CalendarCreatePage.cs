
using atFrameWork2.SeleniumFramework;
using OpenQA.Selenium.Support.UI;

namespace atFrameWork2.PageObjects
{
    public class CalendarCreatePage
    {
        WebItem NameOfCalendar => new WebItem("//input [@name='name']", "Поле Название календаря");
        WebItem PovtorenieCalendar => new WebItem("//div [@class='calendar-field-container']", "Кнопка Повторяемость");
        WebItem RepeatTime => new WebItem("//option [@value='DAILY']", "Вариант повторения Каждый день");
        WebItem ButtonCreate => new WebItem("//button  [@class = 'ui-btn ui-btn-success']", "Кнопка создать");

        public void CreateCalendarDaily()
        {
            //throw new NotImplementedException();
            NameOfCalendar.Click();
            NameOfCalendar.SendKeys("Тест");
            PovtorenieCalendar.Click();
            RepeatTime.Click();
            ButtonCreate.Click();
        }
    }
}