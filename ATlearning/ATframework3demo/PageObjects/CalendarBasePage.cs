
using atFrameWork2.SeleniumFramework;
using OpenQA.Selenium.Support.UI;
namespace atFrameWork2.PageObjects
{
    public class CalendarBasePage
    {
        WebItem CreateButton => new WebItem("//span [@class = 'ui-btn-split ui-btn-success']", "Кнопка создать");
        public CalendarCreatePage AddNewCalendar()
        {
            CreateButton.Click();
            return new CalendarCreatePage();
        }
    }
}